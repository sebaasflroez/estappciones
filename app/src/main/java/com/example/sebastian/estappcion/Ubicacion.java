package com.example.sebastian.estappcion;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

/**
 * Created by sebastian on 21/09/16.
 */
public class Ubicacion implements LocationListener {

    private Context contexto;
    private boolean networkOn;
    LocationManager locationManager;
    String proveedor;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    public Ubicacion(Context contexto) {

        this.contexto = contexto;
        this.locationManager = (LocationManager) contexto.getSystemService(Context.LOCATION_SERVICE);
        this.proveedor = LocationManager.NETWORK_PROVIDER;
        networkOn = locationManager.isProviderEnabled(proveedor);

        if (ActivityCompat.checkSelfPermission(this.contexto, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.contexto, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(contexto,"Permiso de ubicacion denegado", Toast.LENGTH_LONG).show();
            return;
        }

        locationManager.requestLocationUpdates(proveedor, 1000, 1, this);
    }


    public void getLocation() {
        if (networkOn) {
            if (ActivityCompat.checkSelfPermission(this.contexto, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.contexto, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(contexto,"Permiso de ubicacion denegado", Toast.LENGTH_LONG).show();
                return;
            }
            Location location = locationManager.getLastKnownLocation(proveedor);

            if (location!=null){

                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }
        }
    }

    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        return latitude;
    }

    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        return longitude;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
